Basic-RoBots DieHard 2
======================
### Software Enhancements in this fork
  - the introduction of unitTests
  - use of dependency injection 
  - use of [mocks][1]
  - use of [online Markdown editor][2]
  - unittesting command line 
        python -m unittest discover ..\tests -v -f 

### Feature Enhancements in this fork
  
  - implemented `POP` and `PUSH`
  - modified `if ... then ... else ...` grammar
     - now calls gotosub looking for a label
  - added example of if statement
  - robots can `HASFRIEND` to see if robot is infront
  - Energy is produced by power station
  - robots can `ENERGIZE` each other.
  - robots now each have max energy per type
      - Mothership : 10
      - Tipper     : 50
      - Woodcutter : 20
      - ThermalPowerStation : 20 
  	
### Keywords  
  - `PRINT` < someText >: print the text
  - `READ` < reg > : read from a register
  - `LET` < reg > < int >: set contents of register < reg > with < int >	
  - `GOTO` < label > : goto the line with label LABEL < label >
  - `GOTOSUB` < label > : goto the line with expectation of coming back to the line after `GOTOSUB`
  - `RETURN` : return to line after `GOTOSUB` < label >
  - `WHILE` < condition > ,`ENDWHILE` : execute loop until < condition > fails
  - `If < condition > then < label1 > else < label2 >` : if < condition > evaluates true goto < label1 > else goto < label2 > and expect to return.
  - `PUSH` < reg > : put the value of register < reg > onto the stack
  - `POP` <reg> : pop the value on the top of the stack into register < reg >
  - `CALL` < ext_cmd > < reg >: call a robot command, set register < reg > with the result
  - `REM` < comment > : a comment on this line
  - `LABEL` < label > : define a line
  - `INCLUDE` < afile >: copy contents of file < afile > into place
	

###Example use of `if ... then ... else`

	if A==1 then tright else tleft
	
	print back from branch
	goto eof
	
	label tleft
	  call rotleft
	  print rotleft
	  return
	
	label tright
	  call rotright
	  print rotright
	  return
	
	label eof
	print end ifthenelse example

### Example use of include

---
####File1
      PRINT "included me"
####File2
      PRINT "grateful to hear"
####File3
      INCLUDE File1
      INCLUDE File2

####Evaluation of File3
      Print "include me"
      Print "grateful to hear"

---
### Wish list

  - programmed bots have delay between steps to allow for interaction
    - implementation needs improvement, as impacts `let` behaviour
  - energy substation : conduit for powerstation within 10 units
  - mothership costs to move to new location
  - would like to be able to have multiple robots runnning.
  - implementation of data layer
    - enable multiplayer scenario
  - add vim editor features
  - prevent let from taking too long in cmd line 
  - prevent blank lines from being read as code step.

 
 
[1]: http://www.voidspace.org.uk/python/mock/index.html
[2]: http://www.ctrlshift.net/project/markdowneditor/