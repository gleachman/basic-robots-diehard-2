import unittest
import game

class Test_Game(unittest.TestCase):
    pass


class Test_MapCoords(unittest.TestCase):
    
    def setUp(self):
        from game import MapCoords
        self.x = 8
        self.y = 19
        self.size = 30
        self.coords = MapCoords((self.x,self.y),self.size)
    

class Test_When_Up_called(Test_MapCoords):
    def test_should_move_up_one(self):
        
        self.coords.Up()
        
        res = (self.coords.x, self.coords.y)
        self.assertEqual(res,(self.x, self.y - 1),"coord should have reduced y axis by 1")
        
    def test_should_not_move_past_zero(self):
        self.y = 0
        self.coords.y = 0
        self.coords.Up()
        res = (self.coords.x,self.coords.y)
        self.assertEqual((self.x,self.y), res, "coord should not have changed y or x axis")
        

class Test_When_Down_called(Test_MapCoords):
    def test_should_move_down_one(self):
        
        self.coords.Down()
        
        res = (self.coords.x, self.coords.y)
        self.assertEqual(res,(self.x, self.y + 1),"coord should have increased y axis by 1")
        
    def test_should_not_move_past_last_row(self):
        self.y = self.size - 1
        self.coords.y = self.size - 1
        self.coords.Down()
        res = (self.coords.x,self.coords.y)
        self.assertEqual((self.x,self.y), res, "coord should not have changed y or x axis")
        
class Test_When_Left_called(Test_MapCoords):
    def test_should_move_left_one(self):
        
        self.coords.Left()
        
        res = (self.coords.x, self.coords.y)
        self.assertEqual(res,(self.x - 1, self.y),"coord should have reduced x axis by 1")
        
    def test_should_not_move_past_zero(self):
        self.x = 0
        self.coords.x = 0
        self.coords.Left()
        res = (self.coords.x,self.coords.y)
        self.assertEqual((self.x,self.y), res, "coord should not have changed y or x axis")
        

class Test_When_Right_called(Test_MapCoords):
    def test_should_move_right_one(self):
        
        self.coords.Right()
        
        res = (self.coords.x, self.coords.y)
        self.assertEqual(res,(self.x + 1, self.y),"coord should have increased x axis by 1")
        
    def test_should_not_move_past_last_column(self):
        self.x = self.size - 1
        self.coords.x = self.size - 1
        self.coords.Right()
        res = (self.coords.x,self.coords.y)
        self.assertEqual((self.x,self.y), res, "coord should not have changed y or x axis")
        
        