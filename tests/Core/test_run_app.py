import unittest
import os
import json


class Test_Basic_Robot(unittest.TestCase):
    '''
    This test should only be run on it's own.
    At the moment this is to allow to test for bugs by running the app.
    A catch all because I haven't written a good test coverage yet because
    alot of the classes still have too many interdependancies to make testing easy.
    '''
    
    def Test_Main_Runs(self):
        from BASICRoBot import Main
        from events import Input
        from menu import Menu
        from DataLayer.fileSystemLayer import FileSystemAccess
        from worldmap import WorldMap
        from robot.robotResourceManager import RobotResourceManager
        import display
        
        configFile = open("config.json","r+") 
        config =  json.load(configFile)
        configFile.close()
        resources = config['resources']
        
        theDisplay = display.Display(resources)
        images = theDisplay.images
        
        theInput = Input()
        theDAO = FileSystemAccess(os.curdir)
        theMenu = Menu(theInput, theDisplay, theDAO)        
        theWorldMap = WorldMap(theDAO,images)
        theResourceManager = RobotResourceManager()
        mn = Main(theDisplay, theInput, theMenu, theDAO, theWorldMap, theResourceManager)
        mn.run()

