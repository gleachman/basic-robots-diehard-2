
import unittest
import mock
import robot
import env

class test_All(unittest.TestSuite):
    def __init__(self):
        pass
    
class test_when_robotResourceManager_transfers_resource(unittest.TestCase):
    def test_should_move_resource_between_robots_if_owner_can_remove_resource(self):
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot        
        from mock import Mock
        from env import Env

        #Arrange        
        owner = Mock(Robot)
        friend = Mock(Robot)        
        id_item = 1
        qty = 1
        resourceManager = RobotResourceManager()
        owner.del_item.return_value = True
        
        #Act
        res = resourceManager.transfer_resource(owner, friend, id_item, qty)
        
        #Assert
        friend.add_item.assert_called_with(id_item,qty)        
        self.assertEqual(res, True, "resource Manager should have been able to transfer item")

    def test_should_move_resource_between_robots_if_owner_cannot_remove_resource(self):
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot        
        from mock import Mock
        from env import Env
        
        owner = Mock(Robot)
        friend = Mock(Robot)        
        id_item = 1
        qty = 1
        
        resourceManager = RobotResourceManager()
        owner.del_item.return_value = False
        
        res = resourceManager.transfer_resource(owner, friend, id_item, qty)
        
        self.assertTrue(not friend.add_item.called, "friend should not have had add_item called")        
        self.assertTrue(not res, "resource Manager should not have been able to transfer item")

                

class test_when_robotResourceManager_transfers_energy(unittest.TestCase):
    def test_should_move_energy_between_robots_if_rule_succeeds(self):
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot        
        from mock import Mock
        from env import Env
        
        owner = Mock(Robot)
        friend = Mock(Robot)        
        energy = 7
        
        resourceManager = RobotResourceManager()
        resourceManager.rules = {"can_transfer_energy": lambda o, f, e: True}
        res = resourceManager.transfer_energy(owner, friend, energy)
        
        owner.incr_energy.assert_called_with(-energy)
        friend.incr_energy.assert_called_with(energy)        
        self.assertTrue(res, "resource Manager should have been able to transfer energy")
    
    def test_should_not_move_energy_between_robots_if_rule_fails(self):
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot        
        from mock import Mock
        from env import Env
        
        owner = Mock(Robot)
        friend = Mock(Robot)        
        energy = 7
        
        resourceManager = RobotResourceManager()
        resourceManager.rules = {"can_transfer_energy": lambda o, f, e: False}
    
        res = resourceManager.transfer_energy(owner, friend, energy)
        self.assertTrue(not owner.incr_energy.called, "should not have changed owner energy")
        self.assertTrue(not friend.incr_energy.called, "should not have changed friend energy")        
        self.assertTrue(not res, "resource Manager should not have been able to transfer energy")

class test_when_robotResourceManager_converts_wood_to_energy(unittest.TestCase):
    def test_should_increase_energy_and_decrease_wood_if_rule_satisfied(self):
        
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot
        from robot.inventory import Inventory
        from mock import Mock
        
        
        powerStation = Mock(Robot)
        resourceManager  = RobotResourceManager()
        res = { "condition" : True , "energy" : 4, "wood" : 10 }
        rules = {"can_generate_energy" : \
                 lambda ps : res }
        resourceManager.rules = rules
     
        converted = resourceManager.convert_wood_to_energy(powerStation)
        
        powerStation.incr_energy.assert_called_with(res["energy"])
        powerStation.del_item.assert_called_with( 0 , res["wood"])
        self.assertTrue(converted,"powerStation should have been able to convert energy")
    
    def test_should_not_be_able_to_convert_if_no_resources_not_sufficient(self):
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot
        from robot.inventory import Inventory
        from mock import Mock

        #Arrange        
        powerStation = Mock(Robot)
        powerStation.has_excess_resource.return_value = False
        powerStation.energy=Mock(return_value = 100)
        resourceManager  = RobotResourceManager()
        
        #Act
        converted = resourceManager.convert_wood_to_energy(powerStation)
        
        #Assert
        self.assertTrue(not powerStation.incr_energy.called, "powerStation should not have it's energy increased")
        self.assertTrue(not powerStation.add_item.called, "powerStation should not have had wood reduced")
        self.assertTrue(not converted, "powerstation should not have been able to convert")
        
    def test_should_increase_energy_and_decrease_wood_if_rule_fails(self):
        
        from robot.robotResourceManager import RobotResourceManager
        from robot.robot import Robot
        from robot.inventory import Inventory
        from mock import Mock

        #Arrange        
        powerStation = Mock(Robot)
        powerStation.has_excess_resource.return_value = True
        resourceManager  = RobotResourceManager()
        res = { "condition" : False , "energy" : 4, "wood" : 10 }
        rules = {"can_generate_energy" : \
                 lambda ps : res }
        resourceManager.rules = rules
     
        #Act
        converted = resourceManager.convert_wood_to_energy(powerStation)
        
        #Assert
        self.assertTrue(not powerStation.incr_energy.called, "powerStation should not have it's energy increased")
        self.assertTrue(not powerStation.add_item.called, "powerStation should not have had wood reduced")
        self.assertTrue(not converted, "powerstation should not have been able to convert")
