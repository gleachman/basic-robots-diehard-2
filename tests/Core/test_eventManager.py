import unittest
from eventManagement.eventManager import EventManager
from eventManagement.eventListener import IEventListener
from eventManagement.event import Event, TickEvent
from mock import Mock
        

class Test_EventManager(unittest.TestCase):
    def setUp(self):
        self.num_of_listeners = 5
        self.em = EventManager()
        self.eventListeners = [Mock(IEventListener) for i in range(self.num_of_listeners)]        
        self.event = Mock(Event)        
        self.state = 0
        self.assert_not_called = lambda el,i: self.assertTrue(not el.Notify.called,"non state listener (%d) was Notified " % i)
        self.state_pred = lambda i : lambda : self.state == i     
class Test_when_event_manager_posts_an_event(Test_EventManager):


    def test_should_call_notify_on_listeners(self):
        
        [self.em.register_listener(el) for el in self.eventListeners]  
        
        #Act
        self.em.Post(self.event)
        #Assert
        [self.eventListeners[i].Notify.assert_called_with(self.event) for i in range(self.num_of_listeners)]
                
class Test_when_event_manager_registers_listener_with_active_predicate(Test_EventManager):
    def setUp(self):
        Test_EventManager.setUp(self)
        
        
        
    def test_should_only_Notify_active_listeners(self):
        
        
        [self.em.register_listener(el, self.state_pred(i))        
            for i,el in enumerate(self.eventListeners)]
        
        #Act
        self.em.Post(self.event)
        #Assert
        self.eventListeners[self.state].Notify.assert_called_with(self.event)
        [self.assert_not_called(el,i) for i,el in enumerate(self.eventListeners) 
                                      if i != self.state]
        
        
        
class Test_when_changing_state(Test_EventManager):
    
        
    def test_should_switch_which_listener_is_Nofitied(self):
        
        [self.em.register_listener(el,self.state_pred(i)) 
            for i,el in enumerate(self.eventListeners)]
        
        while self.state < self.num_of_listeners:
        
            [el.reset_mock() for el in self.eventListeners]
            #Act
            self.em.Post(self.event)    
            #Assert
            self.eventListeners[self.state].Notify.assert_called_with(self.event)            
            [self.assert_not_called(el,i) for i,el in enumerate(self.eventListeners) 
                                          if i != self.state]
            
            self.state+=1
            
            