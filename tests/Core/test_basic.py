import robot
import unittest
import mock
from robot.basic import Basic
from mock import Mock

class Test_Basic(unittest.TestCase):
    
    
    def setUp(self):
        
        self.b = Basic()

class Test_when_alt_is_called_on_a_line(Test_Basic):
    
    def setUp(self):
        
        Test_Basic.setUp(self)
        self.b.eval_boolean = Mock(return_value = True)
        self.b.goto_return = Mock(return_value = 2)
        self.b.raise_error = Mock(return_value = lambda m,l : m + " " + str(l))
        
    def test_should_call_bool_eval(self):
        
        condition = "condition"
        behaviour = "LABEL_behave"
        opr = self.b.set_opr("if " + condition + " then " + behaviour)[0]
        
        self.b.alt(opr[1::], 1)
        
        self.assertTrue(not self.b.raise_error.called, "Should not have raised an error")
        self.b.eval_boolean.assert_called_with(condition,1)
        self.b.goto_return.assert_called_with(behaviour,1)
        
    def test_should_call_then_label_if_condition_true(self):
        
        condition = "True"
        good_behaviour = "LABEL_behave_correctly_that_is_nice"
        opr = self.b.set_opr("if " + condition + " then " + good_behaviour)[0]
        line = 1
        
        self.b.alt(opr[1::], line)
        
        self.assertTrue(not self.b.raise_error.called, "Should not have raised an error")
        self.b.eval_boolean.assert_called_with(condition,line)
        self.b.goto_return.assert_called_with(good_behaviour,line)
        
    def test_should_call_then_label_if_condition_false(self):
        
        condition = "False"
        self.b.eval_boolean.return_value = False
        do_something_good = "LABEL_I_am_well_behaved"
        do_something_bad = "LABEL_I_was_badly_behaved"
        opr = self.b.set_opr("if " + condition + " then " + do_something_good + " else " + do_something_bad)[0]
        line = 1
        
        self.b.alt(opr[1::], 1)
        
        self.assertTrue(not self.b.raise_error.called, "Should not have raised an error")
        self.b.eval_boolean.assert_called_with(condition,line)
        self.b.goto_return.assert_called_with(do_something_bad,line)

        
class Test_various_eval_boolean_expressions(Test_Basic):
    def setUp(self):
        Test_Basic.setUp(self)
        self.b.mem = {"A":1}        
        
        
    def test_register_for_value(self):        
        res = self.b.eval_boolean("A==1",1)
        self.assertTrue(res, "register A should be tested and have value 1")
    
    def test_simple_boolean_true(self):
        res = self.b.eval_boolean("True",1)
        self.assertTrue(res, "True is True is True is True!!!")
    
    def test_simple_boolean_false(self):
        res = self.b.eval_boolean("False",1)
        self.assertTrue(not res, "False is False is False!!!")
    
    def test_simple_equality(self):
        res = self.b.eval_boolean("1==1",1)
        self.assertTrue(res,"!!!")
        
    def test_true_and_true_condition(self):
        res = self.b.eval_boolean("True and True",1)
        self.assertTrue(res,"True and True is True!")
    def test_true_and_false_condition(self):
        res = self.b.eval_boolean("True and False",1)
        self.assertTrue(not res,"True and False is False!")
    def test_true_or_false_condition(self):
        res = self.b.eval_boolean("True or False",1)
        self.assertTrue(res,"True or False is True")
    def test_false_or_false_condition(self):
        res = self.b.eval_boolean("False or False",1)
        self.assertTrue(not res,"False or False is False")
    
    def test_complex_boolean_true(self):
        res = self.b.eval_boolean("A==1 and (2>4 or True)",1)
        self.assertTrue(res,"complex works")
    def test_complex_boolean_false(self):
        res = self.b.eval_boolean("A==1 and (2<4 and False)",1)
        self.assertTrue(not res,"complex works")
    def test_game_failed_boolean_true(self):
        self.b.mem["B"]=0
        self.b.mem["C"]=0
        res = self.b.eval_boolean("B==1 or C==0",1)
        self.assertTrue(res,"complex works")
     
    
class Test_push_pop(Test_Basic):
    def setUp(self):
        Test_Basic.setUp(self)
        self.b.mem = {}
        
class Test_when_push_called(Test_push_pop):
    
    def test_should_add_value_in_register_onto_stack(self):
        register = "A"
        register_value = 98
        line = 1
        self.b.mem[register] = register_value        
        opr = self.b.set_opr("push " + register )[0]
        
        res_line = self.b.push(opr[1::],line)

        self.assertEqual(self.b.registerStack, [register_value], "stack should contain same value as register")
        self.assertEqual(line, res_line,"should not have changed lines")
        
class Test_when_pop_called(Test_push_pop):
    def test_should_pop_value_from_stack_onto_register(self):
        register = "A"
        register_value = 98
        line = 1
        self.b.registerStack.append(register_value)
        self.b.mem[register]=0
        opr = self.b.set_opr("pop " + register )[0]
        
        res_line = self.b.pop(opr[1::],line)

        self.assertEqual(register_value, self.b.mem["A"], "value should now appear in register")
        self.assertEqual(len(self.b.registerStack),0,"register should have been emptied")
        self.assertEqual(line, res_line,"should not have changed lines")
    
            