import unittest
import events
from pygame.locals import *
import pygame
#TODO: this is crap that I must initialize pygame. need to remove dependancy


class Test_Events(unittest.TestCase):
    def setUp(self):
        pygame.init()
        self.events = events.Input()
        

class Test_when_escape_pressed_and_escaped_called(Test_Events):
    def test_should_return_true(self):
        self.events.key[K_ESCAPE] = True
        
        res = self.events.escape_pressed()
        
        self.assertTrue(res,"escaped should return true")

class Test_when_escape_pressed_twice_and_escaped_called(Test_Events):
    def test_should_return_false(self):
        self.events.key[K_ESCAPE] = True
        
        res = self.events.escape_pressed()
        self.assertTrue(res,"escaped should return true")
        res = self.events.escape_pressed()
        
        self.assertTrue(not res,"escaped should return false")
        
                
class Test_when_escape_not_pressed_and_escaped_called(Test_Events):
    def test_should_return_true(self):
        self.events.key[K_ESCAPE] = False
        
        res = self.events.escape_pressed()
        
        self.assertTrue(not res,"escaped should return true")
        
class Test_when_tab_pressed_and_tabbed_called(Test_Events):
    def test_should_return_true(self):
        self.events.key[K_TAB] = True
        
        res = self.events.tabbed()
        
        self.assertTrue(res,"tabbed should return true")
        
class Test_when_tab_pressed_twice_and_tabbed_called(Test_Events):
    def test_should_return_false(self):
        self.events.key[K_TAB] = True
        
        res = self.events.tabbed()
        self.assertTrue(res,"tabbed should return true")
        res = self.events.tabbed()
        self.assertTrue(not res, "tabbed should return false")
        
class Test_when_ctrl_pressed_and_ctrl_called(Test_Events):
    def test_should_return_true(self):
        self.events.key[K_LCTRL] = True
        res = self.events.ctrl()
        self.assertTrue(res,"CTRL should return true")
        
class Test_when_return_pressed(Test_Events):
    def test_should_return_true(self):
        self.events.key[K_RETURN] = True
        res = self.events.return_pressed()
        self.assertTrue(res,"RETURN should return true") 

class Test_when_return_pressed_twice(Test_Events):
    def test_should_return_false(self):
        self.events.key[K_RETURN] = True
        res = self.events.return_pressed()
        self.assertTrue(res,"RETURN should return true")
        res =  self.events.return_pressed()
        self.assertTrue(not res,"RETURN should return false")
        
class Test_when_arrows_pressed(Test_Events):
    def test_should_return_list_of_pressed_keys(self):
        self.events.key[K_UP] = True
        self.assertEqual(self.events.arrows(),(1,0,0,0),"UP should be pressed")
        self.events.key[K_DOWN] = True
        self.assertEqual(self.events.arrows(),(1,1,0,0),"DOWN should be pressed")
        self.events.key[K_LEFT] = True
        self.assertEqual(self.events.arrows(),(1,1,1,0),"LEFT should be pressed")
        self.events.key[K_RIGHT] = True
        self.assertEqual(self.events.arrows(),(1,1,1,1),"RIGHT should be pressed")
class Test_when_arrows_cleared(Test_Events):
    def test_should_return_zero_list(self):
        self.events.key[K_UP] = self.events.key[K_DOWN] = self.events.key[K_LEFT] = self.events.key[K_RIGHT] = True
        self.events.reset_arrows()
        res = self.events.arrows()
        self.assertEqual(res,(0,0,0,0),"Arrow registers should have been reset")
        