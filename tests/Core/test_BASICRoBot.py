'''
Created on 14 Apr 2013

@author: Gareth
'''
import unittest
import mock
import events
import BASICRoBot

class Test_BASICRoBot(unittest.TestCase):
    def setUp(self):        
        import DataLayer.fileSystemLayer
        from display import Display
        from events import Input
        from mock import Mock
        from menu import Menu
        from DataLayer.fileSystemLayer import FileSystemAccess
        from worldmap import WorldMap    
        from robot.robotResourceManager import RobotResourceManager
        
        theDisplay = Mock(Display)
        theInput = Mock(Input)
        theDAO = Mock(FileSystemAccess)
        theMenu = Mock(Menu, dao = theDAO)
        theWorldmap = Mock(WorldMap, dao = theDAO)
        theResourceManager = RobotResourceManager()
        
        self.mn =  BASICRoBot.Main(theDisplay, theInput, theMenu, theDAO, theWorldmap, theResourceManager)

class Test_when_change_state_called_in_dashboard_state(Test_BASICRoBot):
        
    def test_should_change_to_gameboard_state(self):
        
        self.mn.state = self.mn.DASHBOARD_STATE
        self.mn.change_state()
        self.assertEqual(self.mn.state,self.mn.GAMEBOARD_STATE,"state is not GAMEBOARD")

    

class Test_when_change_state_called_in_gameboard_state(Test_BASICRoBot):
    
    def test_should_change_to_dashboard(self):
        
        self.mn.state = self.mn.GAMEBOARD_STATE
        self.mn.change_state()
        self.assertEqual(self.mn.state,self.mn.DASHBOARD_STATE, "state did not change to DASHBOARD")

class Test_when_still_playing_called_from_main(Test_BASICRoBot):
    def test_should_check_for_quit_conditions_from_main(self):
        
        self.mn.input.has_quit.return_value = False       
        
        self.assertEqual(self.mn.still_playing(), True, "still playing should return True by default")
        self.mn.quit = True
        self.assertEqual(self.mn.still_playing(), False, "game should have quit from main")
        

    def test_should_check_for_quit_conditions_from_input(self):
        
        self.mn.input.has_quit.return_value = False
        self.assertEqual(self.mn.still_playing(), True, "still playing should return True")
        self.mn.input.has_quit.return_value = True
        self.assertEqual(self.mn.still_playing(), False, "game should have quit from main")
        
        self.mn.input.has_quit.assert_called_with()
        
