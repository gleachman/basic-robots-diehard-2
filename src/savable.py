#!/usr/bin/python
## savable.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## <gareth.leachman@yahoo.com>
## 
## Started on  Sat Jun 30 15:34:37 2012 Pierre Surply
## Last update Fri May 3 2013 Gareth Leachman
##

import pygame
from pygame.locals import *

class Savable:
    def save_img(self, path, size, tile_grid, tile_definitions):
        surface = pygame.Surface((size, size))
        for y in range(size):
            for x in range(size):
                tile_index = tile_grid[x][y]
                color = self._tile2color( tile_index, tile_definitions)
                surface.set_at((x, y), color )
                
        pygame.image.save(surface, path)

    def load_img(self, path, size, tile_grid, tile_definitions):
        surface = pygame.image.load(path)
        for y in range(min(size, surface.get_height())):
            for x in range(min(size, surface.get_width())):
                color = tuple(surface.get_at((x, y)))                
                tile_grid[x][y] = self._color2tile(color,tile_definitions)

    
    def _tile2color(self, tile_index, tile_definitions):
        for i in tile_definitions:
            if tile_index == i[1]:
                return pygame.Color(*i[0])
        return pygame.Color(255, 255, 255)
        
    def _color2tile(self, color,  tile_definitions):
        for i in tile_definitions:
            if color == i[0]:
                return i[1]
        return None
