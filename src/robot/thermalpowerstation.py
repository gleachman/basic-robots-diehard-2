##
## thermalpowerstation.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Mon Jul 23 11:55:10 2012 Pierre Surply
## Last update Mon Jul 23 12:19:20 2012 Pierre Surply
##

import robot

class ThermalPowerStation(robot.Robot):
    def __init__(self, new, world, name, env, (x, y), events, dao, resourceManager):
        robot.Robot.__init__(self, new, world, name, env, (x, y), events, dao, resourceManager)
        self.inv.capacity = 8000000
        self.id_sprite = 3
        self.ext_cmd["burn"] = (self.burn, \
                                        "create 5 unit of energy from 10 units of wood and uses 1 unit of energy", \
                                         "1 if the robot has enough power and 10 units of wood, 0 otherwise",\
                                         (1, []),\
                                         {})

    def burn(self):
        return self.resourceManager.convert_wood_to_energy(self)
        
    