'''
Created on 16 Apr 2013

@author: Gareth
'''

class RobotResourceManager(object):
    '''
   '''
    rules = {"can_transfer_energy": lambda o ,f ,e : o.energy > e and f.max_energy > f.energy + e ,
             "can_transfer_resource" : lambda o, i, q : o.del_item( i, q) ,
             "can_generate_energy" : lambda ps : { "condition" : ps.has_excess_resource("Wood",10) and ps.energy > 1 , \
                                                   "energy" : 6, 
                                                   "wood" : 10 } }
     
        
    
    
    def transfer_energy_to_friend(self, owner, energy):
        friend = owner.get_friend()
        if friend is not None :
            return self.transfer_energy(owner, friend, energy)
        return 0
    
    def transfer_energy(self, owner, friend, energy):
        can_transfer = self.rules["can_transfer_energy"](owner, friend, energy)            
        if can_transfer:
            owner.incr_energy(-energy)
            friend.incr_energy(energy)
            return 1
        return 0
    
    
    def transfer_resource_to_friend(self, owner, id_item, qty):
        friend = owner.get_friend()
        if friend is not None :
            return self.transfer_resource(owner, friend, id_item, qty)        
        return 0
    
    def transfer_resource(self, owner, friend, id_item, qty):
        can_transfer = self.rules["can_transfer_resource"](owner, id_item, qty)
        if can_transfer:
            friend.add_item(id_item, qty)
            return 1
        return 0
    
    def convert_wood_to_energy(self, powerStation):                
        res = self.rules["can_generate_energy"](powerStation)        
        if res["condition"]:
            powerStation.incr_energy(res["energy"])
            powerStation.del_item(0, res["wood"])            
            return 1
        else:
            return 0
    

