##
## game.py for BASIC-RoBots 
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## forked by Gareth Leachman
## <grleachman@yahoo.com>
## 
## Started on  Thu Jun 28 15:31:47 2012 Pierre Surply
## Last update Sum Apr 28 2013 Gareth Leachman
##

class MapCoords:
    moves = ("Up","Down","Left","Right","Stay")
    def __init__(self, coords = (0,0), max_size = 32):
        self.size = max_size - 1
        self.x, self.y = coords[0], coords[1]
        
    def Up(self):
        self.y = max(self.y - 1, 0)
    def Down(self):
        self.y = min(self.size, self.y + 1)
    def Left(self):
        self.x = max(0, self.x - 1)
    def Right(self):
        self.x = min(self.size, self.x + 1)
    def Stay(self):
        pass
    def Coords(self):
        return (self.x, self.y)
    
