##
## game.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## forked by Gareth Leachman
## <grleachman@yahoo.com>
## 
## Started on  Thu Jun 28 15:31:47 2012 Pierre Surply
## Last update Sat Apr 27 2013 Gareth Leachman
##

class MapCoords:
    moves = ("Up","Down","Left","Right","Stay")
    def __init__(self, coords = (0,0), max_size = 32):
        self.size = max_size - 1
        self.x, self.y = coords[0], coords[1]
        
    def Up(self):
        self.y = max(self.y - 1, 0)
    def Down(self):
        self.y = min(self.size, self.y + 1)
    def Left(self):
        self.x = max(0, self.x - 1)
    def Right(self):
        self.x = min(self.size, self.x + 1)
    def Stay(self):
        pass
    def Coords(self):
        return (self.x, self.y)
        
    
class Game:
    def __init__(self, display, events, env):
        self._display = display
        self.env = env
        self.events = events
        self.mapCoord = MapCoords((env.get_pos_robot(0)),env.size)
        self.select_robot = None
        self.events.rec_char = False
        self.exit_game = False
        self._show_terminal = False
        self.arrow = 0
    
    def update(self):
        self._show_terminal = False
        
        if self.events.escape_pressed() : 
            self._set_escaped()             
        
        if self.events.tabbed() :
            tab_forward = int(self.events.ctrl())
            tab_backward = -1
            self.incr_select_robot(tab_forward or tab_backward)
                
        self.env.update_robots(self.select_robot, self._display, self.events)
        
        if self.select_robot != None:            
            self._set_robot_pos()
            self._display.render_terminal()
            self._show_terminal = True
        else:
            self.handle_events()

            
        self._render()
        self._display.flip()

    
    def handle_events(self):
        
        if self.events.return_pressed():
            self._robot_selected()
        
        self.arrow = next((i for i,v in enumerate(self.events.arrows())
                        if v),4) 
        
        if not self.events.ctrl():
            self.events.reset_arrows()
            
        getattr(self.mapCoord,self.mapCoord.moves[self.arrow])()
        
    def incr_select_robot(self, incr):
        self.events.rec_char = True       
        robot_num = (self.select_robot or 0) + incr
        self.select_robot = robot_num % len(self.env.robots)
    
    def _set_escaped(self):
        if self.select_robot != None:
            self.select_robot = None
            self.events.rec_char = False
        else:
            self.exit_game = True
    
    def _render(self):
        
        width_factor = 32
        game_start_point = (0,0)
        
        if self._show_terminal :
            width_factor = 64
            game_start_point = (self._display.window.get_width()/2,0)
        
        self.env.render(self._display.window, \
                            (self.mapCoord.x, self.mapCoord.y, \
                                 self._display.window.get_width() / width_factor, \
                                 self._display.window.get_height() / 32), \
                            game_start_point)
        
    def _set_robot_pos(self):
        self.mapCoord = MapCoords(self.env.get_pos_robot(self.select_robot),self.env.size)
            
    def _robot_selected(self):
        for i in range(len(self.env.robots)):
            if self.env.robots[i].at_pos(self.mapCoord.x,self.mapCoord.y):
                self.select_robot = i
                self.events.rec_char = True
                break
    
        
        