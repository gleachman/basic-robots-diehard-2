'''
Created on 15 Apr 2013

@author: Gareth
'''

class DAO(object):
    def __init__(self):
        pass
    def createFolderStructure(self,path):
        pass
    def game_exists(self,game_name):
        pass
    def get_games(self):
        pass
    def get_robots(self,game_name):
        pass
    def listdir(self,path):
        pass
    def mkdir(self,path):
        pass
    def image_exists(self,game_name,x,y):
        pass
    def path_exists(self,path):
        pass
    def path_isdir(self,path):
        pass
    def is_file(self,path):
        pass
    def makedirs(self,path):
        pass
    def get_image_path(self,game_name,image_name):
        pass
    
    
    def open(self,path,mode):
        pass