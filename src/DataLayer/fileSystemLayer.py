'''
Created on 15 Apr 2013

@author: Gareth
'''
import os   
class FileSystemAccess(object):
    
    storage_folder = "/saves/"
    rootFolder = ""     
    robots_folder = "/robots"
    env_folder = "/env/" 
    image_suffix = ".bmp"
    rootFolder = None
    
    
    def __init__(self, curdir = None):
        if curdir is None: curdir = os.curdir
        self.rootFolder = curdir
        
        
    def createFolderStructure(self,game_name):
        os.makedirs(self.rootFolder + self.storage_folder + game_name)
        os.mkdir(self.rootFolder + self.storage_folder + game_name + self.env_folder)
        os.mkdir(self.rootFolder + self.storage_folder + game_name + self.robots_folder)
        
    def game_exists(self,game_name):
        return self.path_exists(self.rootFolder + self.storage_folder + game_name)
    
    def get_games(self):
        if not self.path_exists(self.rootFolder + self.storage_folder):
            self.mkdir(self.rootFolder + self.storage_folder)
        return os.listdir(self.rootFolder + self.storage_folder)

    def get_robots(self,game_name):
        return self.listdir(self.rootFolder + self.storage_folder + game_name + self.robots_folder)
    
    def listdir(self, path):
        return os.listdir(path)
        
    def image_exists(self,game_name,x,y):         
        return self.is_file(self.rootFolder + self.storage_folder + game_name + self.env_folder + str(x) + "_" + str(y) + self.image_suffix)
    
    def path_exists(self, path):
        return os.path.exists(path)
        
    def path_isdir(self, path):
        return os.path.isdir(path)

    def is_file(self,path):
        return os.path.isfile(path)
    
    def makedirs(self,path):
        os.makedirs(path)
        
    def mkdir(self,path):
        os.mkdir(path)
    def get_image_path(self,game_name,image_name):
        return self.rootFolder + self.storage_folder + game_name + self.env_folder + image_name + self.image_suffix
            
    
    def open(self,path,mode='r'):
        return open(path,mode)
    