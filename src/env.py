#!/usr/bin/python
## env.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 13:22:05 2012 Pierre Surply
## Last update Fri Jul 20 19:54:17 2012 Pierre Surply
##

import os
import random

import surface
import noise
import savable
import DataLayer.dao
from robot import mothership, woodcutter

class Env(savable.Savable):
    temp_tiles = [((255, 255, 255, 255), 0, "Grass"),\
                  ((150, 100, 0, 255), 1, "Dirt"),\
                  ((0, 0, 100, 255), 2, "Water"),\
                  ((255, 255, 150, 255), 3, "Sand"),\
                  ((0, 0, 150, 255), 4, "Deep water"),\
                  ((100, 100, 100, 255), 5, "Stone"),\
                  ((255, 0, 0, 255), 6, "Lava"),\
                  ((20, 20, 20, 255), 7, "Gravel"),\
                  ((10, 10, 10, 255), 8, "Volvanic stone"),\
                  ((100, 0, 100, 255), 9, "Meteorite dirt"),\
                  ((100, 0, 255, 255), 10, "Meteorite stone"),\
                  ((101, 101, 101, 255), 11, "Stone")]

    temp_elts = [((0, 100, 0, 255), 0, "Tree"),\
                 ((150, 150, 150, 255), 1, "Stone"),\
                 ((100, 100, 100, 255), 2, "Bricks"),\
                 ((100, 50, 0, 255), 3, "Wood"),\
                 ((100, 0, 50, 255), 4, "Meteorite"), \
                 ((200, 200, 200, 255), 5, "Iron")]

    biomes = [([(0.3, 4),\
                   (0.8,2),\
                   (1.0,3)], []),\
                  ([(0.5, 2),\
                       (0.7, 3),\
                       (0.8, 1),\
                       (1.0, 0)], []),\
                  ([(0.3, 2),\
                       (0.33, 3),\
                       (0.6, 1),\
                       (1.0, 0)], [(0.4, 0)]),\
                  ([(0.1, 2),\
                       (0.2, 1),\
                       (0.4, 11),\
                       (0.6, 5),\
                       (0.7, 7),\
                       (0.8, 8),\
                       (1.0, 6)], [(0.4, 1)]),\
                  ([(0.5, 6),\
                       (0.8, 8),\
                       (1.0, 5)], []),\
                  ([(0.2, 2),\
                       (0.5, 1),\
                       (0.7, 11),\
                       (1.0, 5)], \
                       []),\
                  ([(0.2, 2),\
                       (0.21,3),\
                       (0.4,1),\
                       (0.8,0),\
                       (1.0,5)], [(0.5, 0)]),\
                  ([(0.5, 6),\
                       (0.8, 8),\
                       (1.0, 5)], []),\
                  ([(0.1, 2),\
                       (0.3, 9),\
                       (0.6, 10),\
                       (0.8, 5),\
                       (1.0, 11)], []),\
                  ([(0.4, 4),\
                       (0.8, 2),\
                       (1.0, 3)], []),\
                  ([(0.1, 2),\
                       (0.2, 1),\
                       (0.4, 11),\
                       (0.6, 5),\
                       (0.7, 7),\
                       (0.8, 8),\
                       (1.0, 6)], [])]
    size = 64
    
    def __init__(self, path, biome, (x, y), events, dao, robotResourceManager,images):
        self.init_tile()
        self.dao = dao
        self.robotResourceManager = robotResourceManager
        self.robots = []
        self.running_robot = 0
        self.biome = biome
        self.x = x
        self.y = y
        self.events = events
        self.path = path
        self.path_img = "saves/" + path + "/env/" + str(x) + "_" + str(y) + ".bmp"
        self.path_img_elts = "saves/" + path + "/env/" + str(x) + "_" + str(y) + "_elts.bmp"
        if self.dao.is_file(self.path_img):
            self.load()
        else:
            self.build(biome)
            self.save()
        self.sprite_floor = surface.cut_surface(images["sprite_floor"], (32, 32))
        self.sprite_floor_mini = surface.cut_surface(images["sprite_floor_mini"], (2, 2))
        self.sprite_elts = surface.cut_surface(images['sprite_elts'], (32, 32))
        self.sprite_robots = surface.cut_rotate(images['sprite_robots'], (32, 32))
        self.cursor = images['cursor']
        self.undef = images["undef_surface"]
        
    def init_tile(self):
        self.tile_grid = [0] * self.size
        self.tile_grid = [[0] * self.size for i in self.tile_grid]
        self.tile_elts = [None] * self.size
        self.tile_elts = [[None] * self.size for i in self.tile_elts]

    def load(self):
        self.load_img(self.path_img, self.size, self.tile_grid, self.temp_tiles)
        if self.dao.is_file(self.path_img_elts):
            self.load_img(self.path_img_elts, self.size,self.tile_elts, self.temp_elts)
        else:
            self.build_elts()
            self.save_img(self.path_img_elts, self.size, self.tile_elts, self.temp_elts)
             
    def save(self):
        self.save_img(self.path_img, self.size, self.tile_grid, self.temp_tiles)
        self.save_img(self.path_img_elts, self.size, self.tile_elts, self.temp_elts)
        for i in self.robots:
            i.save()
    
    
    def render(self, surface, (select_x, select_y, cam_w, cam_h), (dx, dy)):
        begin_x = select_x - (cam_w/2)
        begin_y = select_y - (cam_h/2)
        for y in range(max(begin_y, 0), min(begin_y + cam_h, self.size)):
            for x in range(max(begin_x, 0), min(begin_x + cam_w, self.size)):
                surface.blit(self.sprite_floor[self.tile_grid[x][y]], ((x-begin_x)*32 + dx, (y-begin_y)*32 + dy))
                elts = self.tile_elts[x][y]
                if elts != None:
                    surface.blit(self.sprite_elts[elts], ((x-begin_x)*32 + dx, (y-begin_y)*32 + dy))
        for i in self.robots:
            x = i.pos_x
            y = i.pos_y
            if x >= begin_x and \
                    y >= begin_y and \
                    x < begin_x + cam_w and \
                    y < begin_y + cam_h:
                surface.blit(self.render_robot(i), ((x-begin_x)*32 + dx, (y-begin_y)*32 + dy))
        surface.blit(self.cursor, ((select_x-begin_x)*32 + dx, (select_y-begin_y)*32 + dy))
        if begin_x < 0:
            for y in range(0, cam_h):
                for x in range(0, -begin_x):
                    surface.blit(self.undef, (x*32 + dx, y*32 + dy))
        elif begin_x + cam_w > self.size:
            for y in range(0, cam_h):
                for x in range(self.size - begin_x, cam_w):
                    surface.blit(self.undef, (x*32 + dx, y*32 + dy))
        if begin_y < 0:
            for y in range(0, -begin_y):
                for x in range(0, cam_w):
                    surface.blit(self.undef, (x*32 + dx, y*32 + dy))
        elif begin_y + cam_h > self.size:
            for y in range(self.size - begin_y, cam_h):
                for x in range(0, cam_w):
                    surface.blit(self.undef, (x*32 + dx, y*32 + dy))

    def render_robot(self, r):
        return self.sprite_robots[r.id_sprite][r.orient]

    def render_mini(self, surface, (dx, dy)):
        for y in range(self.size):
            for x in range(self.size):
                surface.blit(self.sprite_floor_mini[self.tile_grid[x][y]], (x*2+dx, y*2+dy))

    def build(self, biome):
        n = noise.Noise(33, 33, 8, 8)
        for y in range(self.size):
            for x in range(self.size):
                self.tile_grid[x][y] = self.get_rndtile(x, y, n, biome)
        self.build_elts()
        self.land_mothership()
                
    def get_rndtile(self, x, y, noise, biome):
        val = noise.smooth_noise(x, y, 0.6)
        for i in self.biomes[biome][0]:
            if val < i[0]:
                return i[1]
        return 0

    def build_elts(self):
        biome = self.biome
        n = noise.Noise(33, 33, 8, 8)
        for y in range(self.size):
            for x in range(self.size):
                elt = self.get_rndelts(x, y, n, biome)
                tile = self.tile_grid[x][y]
                self.tile_elts[x][y] = None
                if elt == 0:
                    if tile in [0,1]:
                        self.tile_elts[x][y] = elt
                elif elt == 1:
                    if tile in [0,1, 5, 7, 8, 9, 10, 11]:
                        self.tile_elts[x][y] = elt
                else:
                    self.tile_elts[x][y] = elt
                
    def get_rndelts(self, x, y, noise, biome):
        val = noise.smooth_noise(x, y, 0.7)
        for i in self.biomes[biome][1]:
            if val < i[0]:
                return i[1]
        return None


    def land_mothership(self):
        r = mothership.Mothership(True,\
                                      self.path,\
                                      "Mothership_"+str(self.x)+"-"+str(self.y),\
                                      self, \
                                      (0, 0),\
                                      self.events, self.dao, self.robotResourceManager)
        
        x = random.randint(0, self.size-1)
        y = random.randint(0, self.size-1)
        while r.move_to((x, y)) == 0:
            x = random.randint(0, self.size-1)
            y = random.randint(0, self.size-1)
        self.robots.append(r)

    def get_pos_robot(self, n):
        if n < len(self.robots):
            return self.robots[n].get_pos()
        else:
            return (0,0)

    def get_pos_robots(self):
        return [ i.get_pos() for i in self.robots]
            
    def update_robots(self, n, display, events):
        self.robots[self.running_robot].update((self.running_robot == n), \
                                                   display, events)
        if n != None and self.running_robot != n:
            self.robots[n].update(True, \
                                      display, events)
        self.running_robot += 1
        if self.running_robot >= len(self.robots):
            self.running_robot = 0
    
    def get_friend(self, robot):
        pos = robot.get_pos_forward()
        friends = [ r for r in self.robots if r.get_pos() == pos ]
        if len(friends)!=0:
            return friends[0]
        return None
        
