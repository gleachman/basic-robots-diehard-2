##
## input.py for BASIC-RoBots.py in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## forked by Gareth Leachman
## <grleachman@yahoo.com>
## 
## Started on  Wed May  9 17:54:23 2012 Pierre Surply
## Last update Sat Apr 27 2013 Gareth Leachman
##

import pygame
from pygame.locals import *

from collections import deque

class Input:
    def __init__(self):
        pygame.key.set_repeat(500, 30)
        self.key = [0]*1000
        self.mouse = (0,0)
        self.mouserel = (0,0)
        self.mousebuttons= [0]*9
        self.quit = False
        self.char_stack = deque([])
        self.rec_char = False
        self.resize_display = 0

    def update(self):
        self.resize_display = 0
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                self.key[event.key] = 1;
                if self.rec_char:
                    self.push_char(event.unicode)
            elif event.type == KEYUP:
                self.key[event.key] = 0;
            elif event.type == MOUSEMOTION:
                self.mouse = event.pos
                self.mouserel = event.rel
            elif event.type == MOUSEBUTTONDOWN:
                self.mousebuttons[event.button] = 1
            elif event.type == MOUSEBUTTONUP:
                if event.button != 4 and event.button != 5:
                    self.mousebuttons[event.button] = 0
            elif event.type == QUIT:
                self.quit = True
            elif event.type == VIDEORESIZE:
                self.resize_display = event.size

    def resize_display(self):
        return self.resize_display
    
    def has_quit(self):
        return self.quit

    def push_char(self, c):
        self.char_stack.append(c)
        
    def get_key_once(self, k):
        if self.key[k]:
            self.key[k] = False
            return True
        else:
            return False
        
    def escape_pressed(self):
        return self.get_key_once(K_ESCAPE)

    def tabbed(self):
        return self.get_key_once(K_TAB)

    def ctrl(self):
        return self.key[K_LCTRL] or self.key[K_RCTRL]
    
    def return_pressed(self):
        return self.get_key_once(K_RETURN)
    
    def arrows(self):
        return (self.key[K_UP],self.key[K_DOWN],self.key[K_LEFT],self.key[K_RIGHT])
    
    def reset_arrows(self):
        self.key[K_UP] = self.key[K_DOWN] = self.key[K_LEFT] = self.key[K_RIGHT] = False
    