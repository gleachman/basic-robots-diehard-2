'''
Created on 22 Apr 2013

@author: Gareth
'''

class Event(object):
    pass
class MoveUp(Event):
    pass
class MoveDown(Event):
    pass
class MoveLeft(Event):
    pass
class MoveRight(Event):
    pass
class EscapePressed(Event):
    pass
class ReturnPressed(Event):
    pass
class TickEvent(Event):
    pass