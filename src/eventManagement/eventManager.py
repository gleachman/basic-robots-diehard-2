'''
Created on 22 Apr 2013

@author: Gareth
'''
import eventDispatcher

class EventManager(eventDispatcher.IEventDispatcher):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''
        from weakref import WeakKeyDictionary
        self.listeners = WeakKeyDictionary()
    
    def register_listener(self,listener, is_active = lambda : True):
        self.listeners[listener] = is_active
        
    def unregister_listener(self,listener):
        if listener in self.listeners.keys():
            del self.listeners[listener]
    
    def Post(self,event):
        [ listener.Notify( event ) for listener in self.listeners.keys() 
                                   if self.listeners[listener]() ]
    
    