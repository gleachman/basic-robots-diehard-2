##
## BASICRoBot.py for BASIC-RoBots-diehard 2 
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## Modified by Gareth Leachman
## grleachman@yahoo.com
## 
## Started on  Wed May  9 17:39:28 2012 Pierre Surply
# Last update  Gareth Leachman 
##

import game
import terminal
import dashboard
import pygame
    
class Main:
    def __init__(self, display, user_input, menu, dao, worldmap, resourceManager):
        
        self.dao = dao
        self.display = display
        self.input = user_input
        self.menu = menu        
        self.dashboard = None
        self.resourceManager = resourceManager
        self.worldmap = worldmap
        self.state = self.MAIN_MENU_STATE
        self.game = None
        self.quit = False
        
    
    def still_playing(self):
        return not self.input.has_quit() and not self.quit
    
    def change_state(self):
        if self.state == self.DASHBOARD_STATE:
            self.state = self.GAMEBOARD_STATE
            return
        
        if self.state == self.GAMEBOARD_STATE:
            self.state = self.DASHBOARD_STATE
            return
        # TODO: Need to implement this state change properly.
        if self.state == self.DASHBOARD_STATE:
            self.state = self.MAIN_MENU_STATE
            return
    
    
    def state_command_dashboard(self):
        if self.dashboard.exit_dashboard:            
            self.worldmap.build_env((self.dashboard.select_x,\
                                                  self.dashboard.select_y), \
                                                  self.input,self.resourceManager)
            self.game = game.Game(self.display, self.input, \
                                      self.worldmap.env[self.dashboard.select_x]\
                                      [self.dashboard.select_y])
        
            self.dashboard.exit_dashboard = False
            self.change_state()
            return
            
        self.dashboard.update()
        
    def state_command_game(self):
        if self.game.exit_game:
            self.game.exit_game = False
            self.change_state()
            return
        
        self.game.update()
        
    def state_command_mainmenu(self):
        cmd = self.menu.update()
        if cmd != None:
            self.menu_command(cmd)
        
    def run(self):
        while self.still_playing():
            self.input.update()
            self.resize_display()
            #Run current state command    
            getattr(self,self.state)()
            
        #TODO: throws an exception when load is error
        self.worldmap.save()

    def resize_display(self):
        if self.input.resize_display:
            self.display.resize(self.input.resize_display)
            
    def menu_command(self, cmd):
        fn = self.commands[cmd[0]]
        getattr(self,fn)(cmd[1])
        t = terminal.Terminal(self.input)
        t.set_title("Worldmap")    
        self.dashboard = dashboard.Dashboard(self.worldmap, self.input, self.display, \
                                             t.write, t.clear, t.display)                        
        self.state = self.DASHBOARD_STATE
            
    def quit_game(self, path):
        self.quit = True
        
    #create_path_structure_save_new_map_and_return_world_map
    
    def new_game(self, path):
        self.dao.createFolderStructure(path)
        self.worldmap.initialize(path)
        self.worldmap.save()

    def load_game(self, path):
        self.worldmap.initialize(path)
        self.worldmap.load(self.input, self.resourceManager)


    commands = ("new_game","load_game","quit_game")
    (NEW_GAME, LOAD_GAME, QUIT_GAME) = range(3)
    (MAIN_MENU_STATE, DASHBOARD_STATE, GAMEBOARD_STATE) = \
        ("state_command_mainmenu","state_command_dashboard", "state_command_game")
    

if __name__ == "__main__":
    import display
    import events
    import menu
    import worldmap
    import json
    from DataLayer.fileSystemLayer import FileSystemAccess
    from robot.robotResourceManager import RobotResourceManager

    
    configFile = open("config.json","r+") 
    config =  json.load(configFile)
    configFile.close()
    resources = config['resources']
    
    theDisplay = display.Display(resources)
    images = theDisplay.images
    
    theInput = events.Input()
    theDAO = FileSystemAccess()
    theMenu = menu.Menu(theInput, theDisplay, theDAO)
    theWorldmap = worldmap.WorldMap(theDAO,images)
    theResourceManager = RobotResourceManager()
        
    mn = Main(theDisplay, theInput, theMenu, theDAO, theWorldmap, theResourceManager)
    mn.run()
        