##
## dashboard.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## 
## Started on  Thu Jun 28 21:12:48 2012 Pierre Surply
## Last update Tue Jul  3 11:57:56 2012 Pierre Surply
##

import pygame
from pygame.locals import *

import worldmap
import terminal
from eventManagement.eventListener import *
from eventManagement.event import *


class Dashboard(IEventListener):
    commands = [("ENTER", "land at the selected place"),\
                    (chr(24)+chr(25)+chr(26)+chr(27), "move cursor"),\
                    ("ESC", "cancel")]
    def __init__(self, worldmap, events, displayscreen, say, clear,display):
        if worldmap == None: 
            return
        self.say = say
        self.clear = clear
        self.display = display
        self._display = displayscreen
        self._events = events 
        self.worldmap = worldmap
        self.select_x = 16
        self.select_y = 16
        self.nbr_mothership = 1
        self.exit_dashboard = False
        self.update_term()

    def Notify(self, event):
        return NotImplementedError("Notification will replace update method")

        

    def update(self):
        self.handle_events()
        w = self._display.window.get_width()
        h = self._display.window.get_height()
        self.display(self._display, (0, 0, w/16-1, h/12-1))
        self._display.render_terminal()
        self.worldmap.render(self._display.window, \
                                    (w/2+((w/2 - 256) / 2), 16),\
                                 (self.select_x, self.select_y))
        self._display.flip()
        self.exit_dashboard = self._events.get_key_once(K_RETURN)
        
    def move(self, event):
        new_pos = [sum(coords) for coords in zip(self.movement[event]),(self.select_x, self.select_y)]
    
    movement  = {MoveUp: (0,-1), \
                 MoveDown: (0,1),\
                 MoveLeft: (-1,0), \
                 MoveRight: (1,0)}
    
    def handle_events(self):
        
        if self._events.get_key_once(K_UP) and self.select_y > 0:
            self.select_y -= 1
            self.update_term()
        if (self._events.get_key_once(K_DOWN)) and self.select_y < self.worldmap.size-1:
            self.select_y += 1
            self.update_term()
        if self._events.get_key_once(K_LEFT) and self.select_x > 0:
            self.select_x -= 1
            self.update_term()
        if (self._events.get_key_once(K_RIGHT)) and self.select_x < self.worldmap.size-1:
            self.select_x += 1
            self.update_term()

    def update_term(self):
        self.clear()
        self.say("Please select a landing site for the mothership\n\n")
        self.info_biomes(self.worldmap.tile_grid[self.select_x][self.select_y])
        self.say("\n\n")
        self.info_commands()


    
    def info_biomes(self, n):
        self.say("World\n", 1)
        self.say("  ")
        self.say("Name",1)
        self.say(": " + self.worldmap.game_name + "\n")
        self.say("  ")
        self.say("Mothership",1)
        self.say(": " + str(self.nbr_mothership))
        self.say("\n\nSelected place\n", 1)
        for i in self.worldmap.biomes:
            if i[1] == n:
                self.say("  ")
                self.say("X", 1)
                self.say(": " + str(self.select_x))
                self.say("  ")
                self.say("Y", 1)
                self.say(": " + str(self.select_y))
                self.say("\n  ")
                self.say("Biome", 1)
                self.say(": " + i[2])
                self.say("\n  ")
                self.say("Difficulty", 1)
                dif = self.worldmap.difficulties[i[3]]
                self.say(": ")
                self.say(dif[0], dif[1])

    def info_commands(self):
        self.say("Commands\n", 1)
        for i in self.commands:
            self.say("  ")
            self.say(i[0], 1) 
            self.say(": " + i[1] + "\n")
