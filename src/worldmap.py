##
## worldmap.py for BASIC-RoBots in /home/surply_p
## 
## Made by Pierre Surply
## <pierre.surply@gmail.com>
## <grleachman@yahoo.com>
## 
## Started on  Thu Jun 28 20:42:21 2012 Pierre Surply
## Last update Fri May 3 2013 Gareth Leachman
##


import surface
import savable
import noise
import env

from robot import mothership
from robot import woodcutter
from robot import tipper
from robot import thermalpowerstation

class WorldMap(savable.Savable):
    biomes = [((0,0,255,255), 0, "Sea", 0),\
                  ((100,100,0,255), 1, "Beach", 2),\
                  ((0,255,0,255),2, "Plains", 0),\
                  ((10,10,10,255),3, "Montainous", 2),\
                  ((250,250,250,255),5,"Snowy", 3),\
                  ((0,250,0,255),6, "Forest", 1),\
                  ((100,0,100,255),7, "Corruption", 4),\
                  ((255,0,100,255),8, "Meteorite", 3),\
                  ((0,0,100,255),9, "Deep sea", 2),\
                  ((255,255,0,255),10, "Montainous", 2),\
                  ((255,0,0,255),4, "Volcanic", 4)]
    difficulties = [("Very easy",2),("Easy",3),("Normal",4),("Hard",5),("Very hard",6)]
    
    size = 32
    s = [(0.2, 9),\
             (0.5, 0),\
             (0.55, 1),\
             (0.555,7),\
             (0.56, 8),\
             (0.65, 2),\
             (0.75, 6),\
             (0.83, 3),\
             (0.85, 10),\
             (0.9, 5),\
             (1.0, 4)]

    robot_types = {"Mothership": mothership.Mothership, \
                   "Woodcutter": woodcutter.Woodcutter, \
                   "Tipper": tipper.Tipper, \
                   "TPS": thermalpowerstation.ThermalPowerStation}
     
    def __init__(self, dao,images):
        self.dao = dao
        self.images = images
        self.sprite_biomes = surface.cut_surface(images["sprite_biomes"], (8, 8))
        self.sprite_biomes_maxi = surface.cut_surface(images["sprite_biomes_maxi"], (16, 16))
        self.cursor = images["biome_cursor"]
        self.cursor_max = images['cursor_max']
        
    def initialize(self, game_name):        
        self.game_name = game_name
        self.init_tile()
        self.build()

    def init_tile(self):
        self.tile_grid = [0] * self.size
        self.tile_grid = [[0] * self.size for i in self.tile_grid]
        self.env = [None] * self.size
        self.env = [[None] * self.size for i in self.tile_grid]
        
    def render(self, surface, (dx,dy), (sx, sy)):
        for y in range(self.size):
            for x in range(self.size):
                surface.blit(self.sprite_biomes[self.tile_grid[x][y]], (x*8+dx, y*8+dy))
        surface.blit(self.cursor, (sx*8-1+dx, sy*8-1+dy))
        if self.env[sx][sy] != None:
            self.env[sx][sy].render_mini(surface, (dx-1, (self.size*8) + dy+2))
        for y in range(max(0,sy - 4), min(sy + 4, self.size)):
            for x in range(max(0,sx - 4), min(sx + 4, self.size)):
                surface.blit(self.sprite_biomes_maxi[self.tile_grid[x][y]], \
                                 ((x-(sx-4))*16+128+dx+1, \
                                      (y-(sy-4))*16+256+dy+2))
        surface.blit(self.cursor_max, (dx + 191, dy + 319))

    def build(self):
        self.init_tile()
        n = noise.Noise(33, 33, 4, 8)
        for y in range(self.size):
            for x in range(self.size):
                biome = self.get_rndtile(x, y, n)
                self.tile_grid[x][y] = biome

    def get_rndtile(self, x, y, noise):
        val = noise.smooth_noise(x, y, 0.4)
        for i in self.s:
            if val < i[0]:
                return i[1]
        return self.s[0][1]

    def build_env(self, (x, y), events, resourceManager):
        if self.env[x][y] == None:
            self.env[x][y] = env.Env(self.game_name, \
                                         self.tile_grid[x][y],\
                                         (x, y), events, self.dao, resourceManager,self.images)


    def load(self, events, resourceManager):
        image_path = self.dao.get_image_path(self.game_name, "worldmap")
        self.load_img(image_path, self.size, self.tile_grid,self.biomes)
        
        for y in range(self.size):
            for x in range(self.size):
                if self.dao.image_exists(self.game_name , x , y ):
                    self.env[x][y] = env.Env(self.game_name, \
                                                 self.tile_grid[x][y],\
                                                 (x, y), events, self.dao, resourceManager,self.images)
                    
        
        for i in self.dao.get_robots(self.game_name):
            part = i.split("_")
            coord = [int(x) for x in part[1].split("-")]
            r = self.robot_types[part[0]] ( False, \
                                            self.game_name, \
                                            i, \
                                            self.env[coord[0]][coord[1]], \
                                            (0, 0), \
                                            events, \
                                            self.dao, \
                                            resourceManager )
            
            self.env[coord[0]][coord[1]].robots.append(r)
    
    def save(self):
        img_path = self.dao.get_image_path(self.game_name ,"worldmap")
        self.save_img(img_path, self.size, self.tile_grid, self.biomes)
        
        for y in range(self.size):
            for x in range(self.size):
                if self.env[x][y] != None:
                    self.env[x][y].save()
    
